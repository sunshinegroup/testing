# Hfax 移动端测试项目

基于Appium client的App自动化测试,采用Junit4编写.需要指定如下环境变量

* APP_PATH 指定的APk或ipa的位置
* PLATFORM Android 或者 iOS 的版本

**Android测试命令:

        APP_PATH=/path/to/apk PLATFORM=Android mvn test -Dtest=com.hfax.app.test.AppFeatureTestSuite 
**iOS测试命令: 
        
        APP_PATH=/path/to/ipa  PLATFORM=Android mvn test -Dtest=com.hfax.app.test.AppFeatureTestSuite
### 已经覆盖的测试

>1. 注册
>2. 登录
>3. 忘记密码
>4. 退出登录
>5. 投资券查询
>6. 意见反馈
>7. 待付息产品
>8. 已完成产品
>9. 推荐产品列表显示项目、查看更多
>10. 已购买产品列表显示项目
>11. 热卖产品列表显示项目
>12. 收益率产品列表显示项目
>13. 投资期限产品列表显示项目
>14. 提现
>15. 充值
>16. 新用户绑卡
>17. 修改登录密码
>18. 重置交易密码
>19. 邀请好友
>20. 转入、转出
>21. 消息列表
>22. 活动列表
>23. 预热产品
>24. 申购


### 已知但尚未覆盖的测试

>1. PC端绑卡后,App端验证过程
>2. 投资券兑换功能
>3. 身份认证最后一步流程
>4. 未绑卡用户充值
>5.