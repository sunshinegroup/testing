/*删除银行卡信息*/
delete from cbmain.kna_cacd a where exists(select 1 from cbmain.cif_cust cc,cbmain.kna_cust kc where cc.custno=kc.custno and kc.custac=a.custac and (cc.teleno=&1 or cc.custpt=''||'&2' or cc.idtfno=''||'&3')) or a.cardno=''||'&4';

/*修改核心用户信息：手机、身份证*/
update CBMAIN.CIF_CUST cc set cc.idtfno=dbms_random.string('',20),cc.teleno='13000000000',cc.custpt=dbms_random.string('',20) where cc.teleno=&1 or cc.custpt=''||'&2' or cc.idtfno=''||'&3';

commit;
exit
