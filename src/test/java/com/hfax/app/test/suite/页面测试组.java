package com.hfax.app.test.suite;

import com.hfax.app.test.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by trila on 16/3/23.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        消息列表.class,
        转入转出页面测试.class,
})

public class 页面测试组 {
}
