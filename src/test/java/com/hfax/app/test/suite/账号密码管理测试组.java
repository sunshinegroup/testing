package com.hfax.app.test.suite;

import com.hfax.app.test.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by trila on 16/3/23.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        注册测试.class,
        登录测试.class,
        忘记密码测试.class,
        登出测试.class,
        修改密码测试.class,

})
public class 账号密码管理测试组 {
}
