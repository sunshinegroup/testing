package com.hfax.app.test.suite;

import com.hfax.app.test.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by trila on 16/3/21.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	    权限测试.class,
        注册测试.class,
        绑定投资卷测试.class,
        收益率测试.class,
        意见反馈测试.class,
        已购买产品中已完成列表测试.class,
        忘记密码测试.class,
        热卖产品测试.class,
        转入转出页面测试.class,
        登出测试.class,
        消息列表.class,
        待付息产品列表测试.class,
        首页已购买列表.class,
        投资券页面列表测试.class,
        充值测试.class,
        投资期限排序产品列表.class,
        首页推荐产品列表测试.class,
        注册测试.class,
        分享测试.class,
        申购测试.class,
        提现测试.class,
        修改密码测试.class,
        修改交易密码测试.class
})
public class AppFeatureTestSuite {

}
