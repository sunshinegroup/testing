package com.hfax.app.test;

import java.util.concurrent.TimeUnit;

import com.hfax.app.test.object.UserManager;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import com.hfax.app.test.page.object.AppliedForDetailOnePage;
import com.hfax.app.test.page.object.AppliedForDetailTwoPage;
import com.hfax.app.test.page.object.AppliedForProfileDetailPage;
import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.PasswordPage;
import com.hfax.app.test.page.object.RechargePage;
import com.hfax.app.test.page.object.StartPage;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class 申购测试 extends BaseTest {

	private StartPage mStartPage;
	private LoginPage mLoginPage;
	private HomePage mHomePage;
	private PasswordPage mPasswordPage;
	
	private AppliedForDetailOnePage mAppliedForDetailOnePage;
	private AppliedForDetailTwoPage mAppliedForDetailTwoPage;
	private AppliedForProfileDetailPage mAppliedForProfileDetailPage;
	private RechargePage mRechargePage;

	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		mStartPage = new StartPage(driver);
		mLoginPage = new LoginPage(driver);
		mHomePage = new HomePage(driver);
		mPasswordPage = new PasswordPage(driver);
		mAppliedForDetailOnePage = new AppliedForDetailOnePage(driver);
		mAppliedForProfileDetailPage = new AppliedForProfileDetailPage(driver);
		mAppliedForDetailTwoPage = new AppliedForDetailTwoPage(driver);
		mRechargePage = new RechargePage(driver);
	}

	@Test
	public void testAppliedForTest() throws Exception {
		super.skipGuidancePage();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)),
				mStartPage);
		mStartPage.goToLoginView();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)),
				mLoginPage);
		mLoginPage.login(UserManager.INSTANCE.userWithCertification);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
				mHomePage);	
		mHomePage.goToAppliedForTest();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
				mAppliedForDetailOnePage);
		mAppliedForDetailOnePage.goToProfileTest();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
				mAppliedForProfileDetailPage);
		mAppliedForProfileDetailPage.goToBack();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
				mAppliedForDetailOnePage);
		mAppliedForDetailOnePage.goToBrochureTest();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
				mAppliedForProfileDetailPage);
		mAppliedForProfileDetailPage.goToBack();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
				mAppliedForDetailOnePage);
		mAppliedForDetailOnePage.goToTipsTest();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
				mAppliedForProfileDetailPage);
		mAppliedForProfileDetailPage.goToBack();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
				mAppliedForDetailOnePage);
		mAppliedForDetailOnePage.goToContractTest();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
				mAppliedForProfileDetailPage);
		mAppliedForProfileDetailPage.goToBack();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
				mAppliedForDetailOnePage);
		mAppliedForDetailOnePage.goToAppliedForDetailTwoTest();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
				mAppliedForDetailTwoPage);
		mAppliedForDetailTwoPage.FillAmountTest(900);
		mAppliedForDetailTwoPage.AddVoucherTest();
		mAppliedForDetailTwoPage.AddRaiseTest();
		
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
				mPasswordPage);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mRechargePage);
		if(mAppliedForDetailTwoPage.compare()){
			mAppliedForDetailTwoPage.ConfirmBtnTest();
			mRechargePage.rechargeButton();
			mPasswordPage.inputPassword();
			mAppliedForDetailTwoPage.finish();
			
		}else{
			mAppliedForDetailTwoPage.ConfirmBtnTest();
			mPasswordPage.inputPassword();
		}
	  }
}
