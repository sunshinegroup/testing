package com.hfax.app.test;

import com.hfax.app.test.object.UserManager;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.InvestmentVolumePage;
import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.MenuPage;
import com.hfax.app.test.page.object.StartPage;
import com.hfax.app.test.page.object.UserInvestmentVolumePage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

public class 投资券页面列表测试 extends BaseTest {
	
	private StartPage mStartPage;
	private LoginPage mLoginPage;
	private HomePage mHomePage;
	private MenuPage mMenuPage;
	private InvestmentVolumePage mInvestmentVolumePage;
	private UserInvestmentVolumePage mUserInvestmentVolumePage;
	public void setUp() throws Exception {
		super.setUp();
		
		mStartPage = new StartPage(driver);
	    mLoginPage = new LoginPage(driver);
	    mHomePage = new HomePage(driver);
	    mMenuPage = new MenuPage(driver);
	    mInvestmentVolumePage = new InvestmentVolumePage(driver);
	    mUserInvestmentVolumePage = new UserInvestmentVolumePage(driver);
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
	}
	
	@Test
	public void testQueryInvestment() throws Exception {
		super.skipGuidancePage();		
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mStartPage);
        mStartPage.goToLoginView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mLoginPage);
        mLoginPage.login(UserManager.INSTANCE.userWithCertification);
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(60, TimeUnit.SECONDS)), mHomePage);
        mHomePage.goToMenuView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mMenuPage);
        mMenuPage.goToInvestmentVolumeView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mInvestmentVolumePage);
        if (mInvestmentVolumePage.hasInvestmentVolume()) {
        	mInvestmentVolumePage.goToInvestmentVolumeDetailView();
        	PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mUserInvestmentVolumePage);
        	WebElement investmentVolumeDetailLabel = mUserInvestmentVolumePage.getInvestmentVolumeDetailLabel();
        	assertTrue("我的投资券".equals(investmentVolumeDetailLabel.getText()) || "我的投资券".equals(investmentVolumeDetailLabel.getAttribute("name")));
        }
	}
	
}
