package com.hfax.app.test.page.object;

import org.openqa.selenium.WebElement;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import java.util.List;

public class WealthManagementPage extends BasePage {
	
	@AndroidFindBy(xpath = "//android.view.View[5]/android.view.View[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[4]/UIAStaticText[1]")
	WebElement earningRatioButton;
	@AndroidFindBy(xpath = "//android.view.View[6]/android.view.View[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[5]/UIAStaticText[1]")
	WebElement investmentHorizonButton;
	@AndroidFindBy(xpath = "//android.view.View/android.view.View[@content-desc='抢购']")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[@name='抢购']")
	List<WebElement> productsList;
	@AndroidFindBy(xpath = "//android.widget.ListView/android.view.View[@content-desc='暂无记录']")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[@name='暂无记录']")
	List<WebElement> noRecordLabel;

	public WealthManagementPage(AppiumDriver<?> driver) {
		super(driver);
	}
	
	public void goToEarningRatioPoductListView() {
		earningRatioButton.click();
	}
	
	public void goToInvestmentHorizonProductListView() {
		investmentHorizonButton.click();
	}
	
	public List<WebElement> getProductsList() {
		return productsList;
	}

	public List<WebElement> getNoRecordLabel() {
		return noRecordLabel;
	}

}
