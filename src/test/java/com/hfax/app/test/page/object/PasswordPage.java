package com.hfax.app.test.page.object;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import io.appium.java_client.pagefactory.iOSFindBy;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class PasswordPage extends BasePage {

	@AndroidFindBy(xpath = "//android.view.View/android.view.View[@content-desc = '1']")
	 @iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[@name = '1']")
	private WebElement key1;// 键盘
	@AndroidFindBy(xpath = "//android.view.View/android.view.View[@content-desc = '2']")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[@name = '2']")
	private WebElement key2;// 键盘
	@AndroidFindBy(xpath = "//android.view.View/android.view.View[@content-desc = '3']")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[@name = '3']")
	private WebElement key3;// 键盘
	@AndroidFindBy(xpath = "//android.view.View/android.view.View[@content-desc = '4']")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[@name = '4']")
	private WebElement key4;// 键盘
	@AndroidFindBy(xpath = "//android.view.View/android.view.View[@content-desc = '5']")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[@name = '5']")
	private WebElement key5;// 键盘
	@AndroidFindBy(xpath = "//android.view.View/android.view.View[@content-desc = '6']")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[@name = '6']")
	private WebElement key6;// 键盘
	@AndroidFindBy(xpath = "//android.view.View/android.view.View[@content-desc = '7']")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[@name = '6']")
	private WebElement key7;// 键盘
	@AndroidFindBy(xpath = "//android.view.View/android.view.View[@content-desc = '8']")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[@name = '6']")
	private WebElement key8;// 键盘
	@AndroidFindBy(xpath = "//android.view.View/android.view.View[@content-desc = '9']")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[@name = '6']")
	private WebElement key9;// 键盘
	@AndroidFindBy(xpath = "//android.view.View/android.view.View[@content-desc = '0']")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[@name = '6']")
	private WebElement key0;// 键盘
	
	public PasswordPage(AppiumDriver<?> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void inputPassword() throws Exception {
    	numberClick(key6);
    	numberClick(key6);
    	numberClick(key6);
    	numberClick(key6);
    	numberClick(key6);
    	numberClick(key6);
	}
	
	public void numberClick(WebElement numberText){
		Point location = numberText.getLocation();
		driver.performTouchAction(new TouchAction(driver).tap(location.getX(), location.getY()));
	}
}
