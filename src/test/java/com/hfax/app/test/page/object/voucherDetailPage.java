package com.hfax.app.test.page.object;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.TouchAction;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Point;

public class voucherDetailPage extends BasePage {
	@AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.widget.ListView[1]/android.view.View[2]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[2]")
	WebElement voucherOneBtn;
	public voucherDetailPage(AppiumDriver<?> driver){
		super(driver);
	}
	public void ComfirmVoucher(){
		Point point =voucherOneBtn.getLocation();
    	driver.performTouchAction(new TouchAction(driver).tap(point.x, point.y));
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), this);

	}
}
