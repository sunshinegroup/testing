package com.hfax.app.test.page.object;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.Point;
import io.appium.java_client.TouchAction;

public class AppliedForProfileDetailPage extends BasePage {
	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[1]")
    @iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[1]")
    WebElement backBtn;
	public AppliedForProfileDetailPage(AppiumDriver<?> driver) {
        super(driver);
    }
	public void goToBack(){
		Point Location = backBtn.getLocation();
		driver.performTouchAction(new TouchAction(driver).tap(Location.getX(), Location.getY()));
	}
}
