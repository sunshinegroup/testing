package com.hfax.app.test.page.object;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;
public class PassWordManagerPage extends BasePage  {
	@AndroidFindBy(name = "修改登录密码")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[2]/UIAStaticText[1]")
	WebElement UpdatePassWordBtn;
	@AndroidFindBy(name = "重置交易密码")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[3]/UIAStaticText[1]")
	WebElement UpdatePinCodeBtn;
	public PassWordManagerPage(AppiumDriver<?> driver){
		super(driver);
	}
	public void goToUpdatePassWordTest(){
		UpdatePassWordBtn.click();
	}
	public void goToUpdatePinCodeTest(){
		UpdatePinCodeBtn.click();
	}
}
