package com.hfax.app.test.page.object;

import java.util.Map;

import org.openqa.selenium.WebElement;

import com.hfax.app.test.utils.Platform;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class ForgetPasswordPage extends BasePage {
	
	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[4]/android.widget.EditText[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[1]")
	private WebElement inputPhoneNumber;//手机号
	
	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[4]/android.widget.EditText[2]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[2]")
	private WebElement dentifyingCode;//验证码
	
	@AndroidFindBy(name = "获取验证码")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[2]/UIAStaticText[1]")
	private WebElement getDentifyingCode;
	
	@AndroidFindBy(name = "关闭")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[5]")
	private WebElement alertButton;
	
	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[4]/android.widget.EditText[3]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIASecureTextField[1]")
	private WebElement inputPassword;//输入密码
	
	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[4]/android.widget.EditText[4]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIASecureTextField[2]")
	private WebElement againInputPassword;//再次输入密码
	
	@AndroidFindBy(name = "重置密码")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[1]")
	private WebElement resetButton;
	
	public ForgetPasswordPage(AppiumDriver<?> driver) {
		super(driver);
	}

	public void resetCode(String username,String password){
		for (String context : driver.getContextHandles()) {
			if (context.toLowerCase().contains("webview")) {
				driver.context(context);
			}
		}
		Map<String, String> env = System.getenv();
		final String platform = env.get("PLATFORM");
		inputPhoneNumber.clear();
		inputPhoneNumber.sendKeys(username);
		getDentifyingCode.click();
		alertButton.click();
		dentifyingCode.clear();
		dentifyingCode.sendKeys("1234");
		inputPassword.clear();
		inputPassword.sendKeys(password);
		againInputPassword.clear();
		againInputPassword.sendKeys(password);
		if(platform.equals("android"))
		driver.hideKeyboard();
		resetButton.click();
		alertButton.click();	
	}
}
