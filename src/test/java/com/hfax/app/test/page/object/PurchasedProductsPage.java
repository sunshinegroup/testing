package com.hfax.app.test.page.object;

import org.openqa.selenium.WebElement;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class PurchasedProductsPage extends BasePage {
	
	@AndroidFindBy(name = "待付息产品")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[1]")
	WebElement payInterestProductsButton;
	
	@AndroidFindBy(name = "已完成产品")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[2]")
	WebElement finishedProductsButton;
	
	public PurchasedProductsPage(AppiumDriver<?> driver) {
		super(driver);
	}
	
	public WebElement getPayInterestProductsButton() {
		return payInterestProductsButton;
	}
	
	public WebElement getFinishedProductsButton() {
		return finishedProductsButton;
	}
	
	public void goToFinishedProductsView() {
		finishedProductsButton.click();
	}

}
