package com.hfax.app.test.page.object;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

import com.hfax.app.test.utils.ElementHelper;

/**
 * Created by trila on 16/3/4.
 */
public class BasePage {
	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[1]")
	//@iOSFindBy(xpath = "") //返回按钮路径
	protected WebElement backButton;
	
    protected AppiumDriver<?> driver;

    public BasePage(AppiumDriver<?> driver) {
        this.driver = driver;
    }

    public void clickAt(WebElement element) {
    	Point center = ElementHelper.getCenter(element);
    	System.out.print(center);
        driver.performTouchAction(new TouchAction(driver)).tap(center.x, center.y).perform();
    }
    
    public void backButtonClick(){
    	backButton.click();
    }   
}
