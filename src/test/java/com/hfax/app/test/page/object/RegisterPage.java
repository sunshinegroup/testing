package com.hfax.app.test.page.object;

import com.hfax.app.test.object.User;
import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class RegisterPage extends BasePage {

	
	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[2]/android.widget.EditText[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[1]")
	private WebElement phoneNumber;//手机号
	
	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[2]/android.widget.EditText[2]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIASecureTextField[1]")
	private WebElement password;//密码

	@AndroidFindBy(name = "获取验证码")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[2]/UIAStaticText[1]")
	private WebElement getCode;//获取验证码
	
	@AndroidFindBy(name = "关闭")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[6]")
	private WebElement alertButton;//获取验证码

	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[2]/android.widget.EditText[3]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[2]")
	private WebElement code;//验证码

	@AndroidFindBy(name = "注册")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[1]")
	private WebElement registerButton;
	

	public RegisterPage(AppiumDriver<?> driver) {
		super(driver);
	}

	public void register(User user) {
		register(user.getPhone(), user.getPassword());
	}

	public void register(String username, String userpassword){
		for (String context : driver.getContextHandles()) {
			if (context.toLowerCase().contains("webview")) {
				driver.context(context);
			}
		}
		phoneNumber.clear();
		phoneNumber.sendKeys(username);
		password.clear();
		password.sendKeys(userpassword);
		getCode.click();
		alertButton.click();
		code.clear();
		code.sendKeys("1234");
		registerButton.click();
	}
}