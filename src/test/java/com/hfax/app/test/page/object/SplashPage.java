package com.hfax.app.test.page.object;

import com.hfax.app.test.utils.ElementHelper;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

/**
 * Created by trila on 16/3/10.
 */
public class SplashPage extends BasePage {

    @iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAImage[1]")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.LinearLayout[1]")
    WebElement slider;

    public SplashPage(AppiumDriver<?> driver) {
        super(driver);
    }

    public void skipGuidancePage() {
        Point sliderLocation = ElementHelper.getCenter(slider);
        int startX = sliderLocation.getX() * 2 - 50;
        int startY = sliderLocation.getY();
        int endX = sliderLocation.getX() / 10;
        int endY = sliderLocation.getY();
        driver.swipe(startX, startY, endX, endY, 500);
        driver.swipe(startX, startY, endX, endY, 500);
        driver.swipe(startX, startY, endX, endY, 500);
        slider.click();
    }
}
