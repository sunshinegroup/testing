package com.hfax.app.test.page.object;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

import com.hfax.app.test.BaseTest;

public class FeedBackPage extends BasePage {

	@AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.EditText[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATextView[1]")
	WebElement contentTextView;
	@AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.Button[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAButton[1]")
	WebElement submitButton;
	@AndroidFindBy(name = "好的")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAAlert[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[1]")
	WebElement alertButton;
	
	
	
	public FeedBackPage(AppiumDriver<?> driver) {
		super(driver);
	}
	
	public void inputSuggestionAndSubmit(String suggestion) {
		contentTextView.clear();
	    contentTextView.sendKeys(suggestion);
	    submitButton.click();
	    if(!BaseTest.isAndroid()){
	    	 alertButton.click(); 	
	    }
	}
	
}
