package com.hfax.app.test.page.object;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;
public class AppliedForDetailOnePage extends BasePage {
	@AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[1]/android.view.View[18]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[3]")
	WebElement ProfileBtn;
	@AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[1]/android.view.View[19]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[4]")
	WebElement BrochureBtn;
	@AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[1]/android.view.View[20]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[5]")
	WebElement TipsBtn;
	@AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[1]/android.view.View[21]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[6]")
	WebElement ContractBtn;
	@AndroidFindBy(name = "申购")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[1]")
	WebElement AppliedForBtn;
	public AppliedForDetailOnePage(AppiumDriver<?> driver){
		super(driver);
	}
	public void goToBrochureTest() throws Exception{
		 BrochureBtn.click();
	}
	public void goToTipsTest() throws Exception{
		 TipsBtn.click();
	}
	public void goToContractTest() throws Exception{
		ContractBtn.click();
	}
	public void goToProfileTest() throws Exception{
		 ProfileBtn.click();
	}
	public void goToAppliedForDetailTwoTest() throws Exception{
		AppliedForBtn.click();
	}
	
}
