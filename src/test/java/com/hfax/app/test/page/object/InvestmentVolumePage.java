package com.hfax.app.test.page.object;

import java.util.List;
import org.openqa.selenium.WebElement;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class InvestmentVolumePage extends BasePage {
	
	@AndroidFindBy(name = "绑定")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[2]/UIAStaticText[1]")
	WebElement bindButton;
	@AndroidFindBy(xpath = "//android.widget.ListView/android.view.View")
	@iOSFindBy(className = "UIALink")
	List<WebElement> investmentVolumesList;
	
	public InvestmentVolumePage(AppiumDriver<?> driver) {
		super(driver);
	}
	
	public void goToBindInvestmentVolumeView() {
		bindButton.click();
	}
	
	public List<WebElement> getInvestmentVolumesList() {
		return investmentVolumesList;
	}
	
	public boolean hasInvestmentVolume() {
		if(investmentVolumesList.size() > 2) {
			 return true;
		 }
		return false;
	}
	
	public void goToInvestmentVolumeDetailView() {
		investmentVolumesList.get(2).click();
	}


}
