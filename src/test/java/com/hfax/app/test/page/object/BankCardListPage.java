package com.hfax.app.test.page.object;

/**
 * Created by zhangchi on 16/3/28.
 */
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

import java.util.List;

import org.apache.xpath.operations.Bool;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
public class BankCardListPage extends BasePage{
    @AndroidFindBy(xpath = "//android.view.View[4]/android.widget.EditText[1]")
    @iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[2]/UIALink[2]/UIALink[1]/UIALink[2]/UIAStaticText[1]")
    WebElement BankCardNumText;
    public BankCardListPage(AppiumDriver<?> driver){
        super(driver);
    }
    public void checkCardNum(String numStr){
        Assert.assertTrue(BankCardNumText.getText().equals(numStr));
    }

}
