package com.hfax.app.test.page.object;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;
import com.hfax.app.test.BaseTest;

import java.util.List;
import org.openqa.selenium.Point;

public class HomePage extends BasePage {
	@AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[1]/android.view.View[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[1]/UIAStaticText[1]")
	WebElement menuButton;
	@AndroidFindBy(xpath = " //android.webkit.WebView[1]/android.widget.ListView[1]/android.view.View[1]/android.view.View[5]/android.view.View")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[1]")
	WebElement appliedForButton;
	@AndroidFindBy(xpath = "//android.view.View[5]/android.view.View[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[8]/UIALink[1]/UIAStaticText[1]")
	WebElement appliedForButton2;
	@AndroidFindBy(xpath = "//android.view.View[6]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[5]/UIALink[1]/UIAStaticText[1]")
	WebElement balanceBtn;
	@AndroidFindBy(xpath = "//android.widget.LinearLayout[2]/android.widget.ImageView[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATabBar[1]/UIAButton[2]")
	WebElement wealthManagementButton;
	@AndroidFindBy(xpath = "//android.view.View[9]/android.view.View[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[@name='查看更多']")
	WebElement checkMoreButton;
	@AndroidFindBy(xpath = "//android.view.View[8]/android.view.View[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[7]/UIAStaticText[1]")
	WebElement purchasedProductsButton;
	@AndroidFindBy(xpath = "//android.widget.ListView[1]/android.view.View")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[@name='暂无记录']")
	List<WebElement> noRecordLabel;
	@AndroidFindBy(xpath = "//android.view.View/android.view.View[@content-desc='募资中 Link']")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[@name='募资中']")
	List<WebElement> purchasedProudctsList;
	@AndroidFindBy(xpath = "//android.view.View[5]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[4]/UIALink[1]/UIAStaticText[1]")
	WebElement expectedEarningButton;
	@AndroidFindBy(xpath = "//android.widget.TabWidget[1]/android.widget.LinearLayout[1]/android.widget.ImageView[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATabBar[1]/UIAButton[1]")
	WebElement homeButton;
	@AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[4]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[3]/UIALink[1]/UIAStaticText[1]")
	WebElement investTotalAmount;
	
	public HomePage(AppiumDriver<?> driver) {
		super(driver);
	}
	
	public List<WebElement> getNoRecordLabel() {
		return noRecordLabel;
	}
	
	public List<WebElement> getPurchasedProudctsList() {
		return purchasedProudctsList;
	}

	public String getInvestTotalAmount() {
        String money;
		if(BaseTest.isIOS()) {
            money = investTotalAmount.getText();
		}else{
            money = investTotalAmount.getAttribute("name");
            money = money.substring(0,money.lastIndexOf('.') + 3);
		}
        return money;
	}

	public void goToMenuView(){
		Point location = menuButton.getLocation();
		driver.performTouchAction(new TouchAction(driver).tap(location.getX(), location.getY()));
	
	}
	public void goToAppliedForTest() throws Exception{
		  // this.clickAt(appliedForButton2);
		appliedForButton2.click();
	}
	
	public void goToBalanceTest() throws Exception{
		balanceBtn.click();
	}
	public boolean compare(Float amount){
		//判断输入金额与余额是否相同
		String money = BaseTest.isAndroid() ? balanceBtn.getAttribute("name") : balanceBtn.getText();
		boolean flag = Float.parseFloat(money) == amount?true:false;
		return flag;
	}
	public void goToWealthManagementView() {
		wealthManagementButton.click();
	}
	
	public void  goToMoreProductsView() {
		checkMoreButton.click();
	}

	public void goToPurchasedProductsView() {
		purchasedProductsButton.click();
	}
	
	public void goToExpectedEarningDetailView() {
		expectedEarningButton.click();
	}

	public void goToHomePageView() { homeButton.click();}

}
