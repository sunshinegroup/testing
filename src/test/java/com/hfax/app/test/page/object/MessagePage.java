package com.hfax.app.test.page.object;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;

import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class MessagePage extends BasePage {

	@AndroidFindBy(xpath = "//android.widget.TabWidget[1]/android.widget.LinearLayout[3]/android.widget.ImageView[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[2]")
	private WebElement mMessageButton;

	@AndroidFindBy(xpath = "//android.view.View[4]/android.view.View[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[4]")
	private WebElement mActivity;

	@AndroidFindBy(xpath = "//android.widget.ListView[1]/android.view.View[1]/android.view.View[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[4]")
	private WebElement mActivityButton;
	
	public MessagePage(AppiumDriver<?> driver) {
        super(driver);
    }

	public void message()  throws Exception {
		path(mMessageButton);
		path(mActivity);
		path(mActivityButton);
	}
	
	public void path(WebElement pathWebElement) {
		Point location = pathWebElement.getLocation();
		driver.performTouchAction(new TouchAction(driver).tap(location.getX(), location.getY()));
	}
}
