package com.hfax.app.test.page.object;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class RechargePage extends BasePage {

	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[3]/android.widget.EditText[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[9]")
	private WebElement mRechargeAmount;

	@AndroidFindBy(name = "确认充值")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[5]")
	private WebElement mRechargeButton;
	
	@AndroidFindBy(name = "关闭")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[4]")
	private WebElement mRechargeError;

	@AndroidFindBy(name = "确定")
//	@iOSFindBy(xpat="")
	private WebElement mSuccessButton;
	public RechargePage(AppiumDriver<?> driver) {
		super(driver);
	}

	public void recharge(int account) {
		Point location = mRechargeAmount.getLocation();
		driver.performTouchAction(new TouchAction(driver).tap(location.getX(), location.getY()));
		mRechargeAmount.sendKeys(String.valueOf(account));

		if (account == 0){
			mRechargeButton.click();
			mRechargeError.click();
		}else{
			mRechargeButton.click();
		}
	}
	
	
	public void successButton(){
		mRechargeError.click();
	}
	
	public void rechargeButton(){
		mRechargeButton.click();
	}

}
