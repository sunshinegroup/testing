package com.hfax.app.test.page.object;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.junit.Assert;
import org.openqa.selenium.WebElement;

import com.hfax.app.test.BaseTest;
public class updatePassWordPage extends BasePage  {
	@AndroidFindBy(xpath = "//android.widget.EditText[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIASecureTextField[1]")
	WebElement oldPasswordText;
	@AndroidFindBy(xpath = "//android.widget.EditText[2]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIASecureTextField[2]")
	WebElement newPasswordText;
	@AndroidFindBy(xpath = "//android.widget.EditText[3]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIASecureTextField[3]")
	WebElement confirmNewPasswordText;
	@AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[1]/android.view.View[3]/android.widget.Button[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[1]")
	WebElement confirmButton;
	@AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[3]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[3]")
	WebElement popTitleLabel;
	@AndroidFindBy(name = "关闭")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[4]")
	WebElement closeButton;
    

	public updatePassWordPage(AppiumDriver<?> driver){
		super(driver);
	}
	public void goToUpdatePassWordTest(String oldPassword,String newPassword){
		oldPasswordText.clear();
	 	oldPasswordText.sendKeys(oldPassword);
	 	newPasswordText.clear();
	 	newPasswordText.sendKeys(newPassword);
	 	confirmNewPasswordText.clear();
	 	confirmNewPasswordText.sendKeys(newPassword);
	 	confirmButton.click();
	}
	public void closePopViewTest(){
		if(BaseTest.isIOS()){
			Assert.assertTrue(popTitleLabel.getText().equals("重置密码成功"));
		}else{
			Assert.assertTrue(popTitleLabel.getAttribute("name").equals("重置密码成功"));
		}
		closeButton.click();
		backButtonClick();
	}
}