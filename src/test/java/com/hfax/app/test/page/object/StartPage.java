package com.hfax.app.test.page.object;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

/**
 * Created by trila on 16/3/4.
 */
public class StartPage extends BasePage {
	@AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.Button[2]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAButton[2]")
	private WebElement mLoginButton;
	@AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.Button[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAButton[1]")
	private WebElement mRegisterButton;

	public StartPage(AppiumDriver<?> driver) {
		super(driver);
	}

	public WebElement getLoginButton() {
		return mLoginButton;
	}

	public void setLoginButton(WebElement loginButton) {
		mLoginButton = loginButton;
	}

	public WebElement getRegisterButton() {
		return mRegisterButton;
	}

	public void setRegisterButton(WebElement registerButton) {
		mRegisterButton = registerButton;
	}

	public void goToLoginView() {
		mLoginButton.click();
	}

	public void goToRegisterView() {
		mRegisterButton.click();
	}
}
