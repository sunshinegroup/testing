package com.hfax.app.test.page.object;

import static junit.framework.TestCase.assertTrue;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

import com.hfax.app.test.BaseTest;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class SharePage extends BasePage {
	@AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[7]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[4]")
	private WebElement mShareButton;

	@AndroidFindBy(xpath = "//android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.ImageView[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAButton[2]")
	private WebElement mWeChat;

	@AndroidFindBy(xpath = "//android.widget.FrameLayout[2]/android.widget.LinearLayout[1]/android.widget.ImageView[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAButton[3]")
	private WebElement mCircleOfFriends;

	@AndroidFindBy(xpath = "//android.widget.FrameLayout[3]/android.widget.LinearLayout[1]/android.widget.ImageView[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAButton[4]")
	private WebElement mSMS;

	//@AndroidFindBy(xpath = "")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[2]")
	private WebElement mWeChatBack;

	//@AndroidFindBy(xpath = "")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[5]")
	private WebElement mWeChatCell;

	//@AndroidFindBy(xpath = "")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[4]/UIAImage[1]/UIAButton[1]")
	private WebElement cancel;
	
	@AndroidFindBy(xpath = "//android.widget.FrameLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.widget.ImageView[1]")
	//@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[4]/UIAImage[1]/UIAButton[1]")
	private WebElement mSMSBack;

	@AndroidFindBy(xpath = "//android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAStaticText[1]")
	private WebElement mWeChatNameText;

	@AndroidFindBy(xpath = "//android.widget.FrameLayout[2]/android.widget.LinearLayout[1]/android.widget.TextView[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAStaticText[2]")
	private WebElement mCircleOfFriendsNameText;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout[3]/android.widget.LinearLayout[1]/android.widget.TextView[1]")
    @iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAStaticText[3]")
	private WebElement mSMSNameText;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.EditText[1]")
    @iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIATextField[1]")
	private WebElement mSMSRecipient;

	@AndroidFindBy(name = "发送")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[2]/UIAElement[1]/UIAButton[2]")
	private WebElement mSMSSendButton;

	public SharePage(AppiumDriver<?> driver) {
		super(driver);
	}

	public void path(WebElement pathWebElement) {
		Point location = pathWebElement.getLocation();
		driver.performTouchAction(new TouchAction(driver).tap(location.getX(), location.getY()));
	}

	public void shareTest() {
		mShareButton.click();
		assertTrue(mWeChatNameText.getText().equals("微信")||mWeChatNameText.getAttribute("name").equals("微信"));
		assertTrue(mCircleOfFriendsNameText.getText().equals("朋友圈")||mCircleOfFriendsNameText.getAttribute("name").equals("朋友圈"));
		assertTrue(mSMSNameText.getText().equals("短信")||mSMSNameText.getAttribute("name").equals("短信"));

		// 分享微信 （目前跳转到微信跳转不回来）
		// mWeChat.click();
		// mCircleOfFriends.click();

		//短信分享
		mSMS.click();
		mSMSRecipient.sendKeys("13333333333");
		if(BaseTest.isAndroid()){
			driver.hideKeyboard();
		}
		assertTrue(mSMSSendButton.getText().equals("发送")||mSMSSendButton.getAttribute("name").equals("发送"));
		mSMSSendButton.click();
		//短信分享成功后返回
		mSMSBack.click();
	}
}
