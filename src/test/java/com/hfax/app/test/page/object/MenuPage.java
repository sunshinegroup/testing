package com.hfax.app.test.page.object;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

public class MenuPage extends BasePage {
    @AndroidFindBy(xpath = "//android.support.v7.widget.RecyclerView[1]/android.widget.LinearLayout[1]/android.widget.ImageView[1]")
    @iOSFindBy(xpath = "")
    WebElement returnHomePage;  //侧滑菜单中返回首页按钮
	@AndroidFindBy(name = "退出登陆")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAStaticText[2]")
	WebElement logoutButton;
	@AndroidFindBy(name = "意见反馈")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[8]")
	WebElement feedBackButton;
	@AndroidFindBy(name = "密码管理")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[6]")
	WebElement passwordManagerButton;
	@AndroidFindBy(name = "客服电话")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[10]")
	WebElement customServiceButton;
	@AndroidFindBy(xpath = "//android.widget.LinearLayout[2]/android.widget.Button[@text = '允许']")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAAlert[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[1]")
	WebElement customServiceTips; // 客服提示按钮
	@AndroidFindBy(name = "提现")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[2]")
	WebElement cashingBtn; // 提现按钮
	@AndroidFindBy(name = "投资券")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[3]")
	WebElement investmentVolumeButton;
	@AndroidFindBy(name = "邀请好友")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[9]")
	WebElement shareButton;
	@AndroidFindBy(name = "充值")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]")
	private WebElement rechargeButton;
	@AndroidFindBy(name = "银行卡管理")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[5]")
	private WebElement bankCardButton;

	public MenuPage(AppiumDriver<?> driver) {
		super(driver);
	}

	public void logout() {
		logoutButton.click();
	}

	public void goToFeedBackView() {
		feedBackButton.click();
	}

	public void goToInvestmentVolumeView() {
		investmentVolumeButton.click();
	}

	public void gotoPasswordManager() {
        passwordManagerButton.click();
	}

	public void gotoCustomService() {
		customServiceButton.click();
		customServiceTips.click();
	}

	public void gotoCashing() {
		cashingBtn.click();
	}

	public void share() {
		shareButton.click();
	}

	public void gotoRecharge() {
		rechargeButton.click();
	}

	public void gotoBank() {
		bankCardButton.click();
	}

    public void gotuHomePage(){
        returnHomePage.click();
    }
}
