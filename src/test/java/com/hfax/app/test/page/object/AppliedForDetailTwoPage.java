package com.hfax.app.test.page.object;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

import org.openqa.selenium.WebElement;

import com.hfax.app.test.BaseTest;


public class AppliedForDetailTwoPage extends BasePage {
	@AndroidFindBy(xpath = "//android.widget.EditText[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[1]")
	WebElement AmountText;
	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[8]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[2]/UIALink[1]/UIAStaticText[1]")
	WebElement voucherCountLabel;
	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[3]/android.view.View[7]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[2]/UIALink[1]/UIAStaticText[1]")
	WebElement voucherBtn;
	@AndroidFindBy(name = "关闭")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[21]")
	WebElement voucherPointBtn;
	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[3]/android.view.View[10]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[3]/UIALink[1]/UIAStaticText[1]")
	WebElement raiseCountLabel;
	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[3]/android.view.View[10]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[3]/UIALink[1]/UIAStaticText[1]")
	WebElement raiseBtn;
	@AndroidFindBy(name = "关闭")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[21]")
	WebElement raisePointBtn;
	@AndroidFindBy(name = "确认申购")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[18]")
	WebElement ConfirmBtn;
	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[3]/android.view.View[2]")
	 @iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[3]")
	WebElement balance;// 余额
	@AndroidFindBy(xpath = "//android.widget.FrameLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[3]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[20]")
	WebElement pointRealnameLabel; // 请实名认证提示语
	@AndroidFindBy(name = "关闭")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[21]")
	WebElement pointBtn; //请实名认证提示关闭按钮
	private float inputAmount;
	public AppliedForDetailTwoPage(AppiumDriver<?> driver) {
		super(driver);
	}

	public void FillAmountTest(int amount) {
		AmountText.clear();
		inputAmount = amount;
		AmountText.sendKeys(String.valueOf(amount));
	}

	public void AddVoucherTest() {
		String usableVoucher = BaseTest.isAndroid() ? voucherBtn.getAttribute("name") : voucherBtn.getText();
		if (usableVoucher.equals("0张可用")) {
			voucherBtn.click();
			voucherPointBtn.click();
		} else {
			voucherBtn.click();
		}
	}

	public void AddRaiseTest() {
		String usableRaiseCountLable = BaseTest.isAndroid() ? raiseCountLabel.getAttribute("name") : raiseCountLabel.getText();
		if (usableRaiseCountLable.equals("0张可用")) {
			raiseBtn.click();
			raisePointBtn.click();
		} else {
			raiseBtn.click();
		}
	}

	public boolean compare(){
		//判断申购金额是否小于可用余额
		String money = BaseTest.isAndroid() ? balance.getAttribute("name") : balance.getText();
		boolean flag = Float.parseFloat(money) < inputAmount ? true : false;
		return flag;
	}
	
	public void ConfirmBtnTest() {
		if (compare()) {
			ConfirmBtn.click();
			raisePointBtn.click();
		}else{
			ConfirmBtn.click();
		}
	}
	public void finish(){
		raisePointBtn.click();
	}
	public void closePoint(){
			pointBtn.click();
	}
}
