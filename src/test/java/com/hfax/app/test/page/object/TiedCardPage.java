package com.hfax.app.test.page.object;

import io.appium.java_client.TouchAction;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

import com.hfax.app.test.object.User;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class TiedCardPage extends BasePage {

	@AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[3]/android.widget.EditText[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[1]")
	private WebElement mNameField;

	@AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[3]/android.widget.EditText[2]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[2]")
	private WebElement mIDField;

	@AndroidFindBy(name = "认证")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[1]")
	private WebElement mConfirmButton;

	@AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[1]/android.view.View[3]/android.widget.Button[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[1]")
	private WebElement mBankList;

	@AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.widget.ListView[1]/android.view.View[2]/android.view.View[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[2]")
	private WebElement mBankName;

	@AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[1]/android.view.View[3]/android.widget.EditText[3]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[3]")
	private WebElement mBankCardNumber;

	@AndroidFindBy(name = "下一步")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[9]")
	private WebElement mNext;

	@AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[2]/android.widget.Button[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[1]")
	private WebElement mComplete;

	public TiedCardPage(AppiumDriver<?> driver) {
		super(driver);
	}

	public void tiedCard(final User user) {
		mNameField.clear();
		mNameField.sendKeys(user.getName());
		mIDField.clear();
		mIDField.sendKeys(user.getID());
		mConfirmButton.click();
	}

	public void bindingBank(String bankCardNumber){
		mBankList.click();
		path(mBankName);
		mBankCardNumber.clear();
		mBankCardNumber.sendKeys(bankCardNumber);
		mNext.click();
		mComplete.click();

	}

	public void path(WebElement pathWebElement) {
		Point location = pathWebElement.getLocation();
		driver.performTouchAction(new TouchAction(driver).tap(location.getX(), location.getY()));
	}
}
