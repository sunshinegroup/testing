package com.hfax.app.test.page.object;

import com.hfax.app.test.object.User;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

/**
 * Created by trila on 16/3/4.
 */
public class LoginPage extends BasePage {

	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[2]/android.widget.EditText[2]")
    @iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIASecureTextField[1]")
    WebElement passwordTextView;
    @AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[2]/android.widget.EditText[1]")
    @iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[1]")
    WebElement phoneTextView;
    @AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[2]/android.view.View[1]")
    @iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[1]")
    WebElement loginButton;
    @AndroidFindBy(name = "忘记密码？")
    @iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[3]/UIAStaticText[1]")
    WebElement forgetButton;

	public LoginPage(AppiumDriver<?> driver) {
		super(driver);
	}

    protected void login(final String userName, final String password) throws Exception {
        phoneTextView.clear();
        phoneTextView.sendKeys(userName);
        passwordTextView.clear();
        passwordTextView.sendKeys(password);
        loginButton.click();
    }

    public void login(final User user) throws Exception {
        login(user.getPhone(), user.getPassword());
    }
    
    public void goToForgetPasswordView() {
    	forgetButton.click();
    }
}
