package com.hfax.app.test.page.object;

import org.openqa.selenium.WebElement;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class BindInvestmentVolumePage extends BasePage {
	
	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[5]/android.widget.EditText[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[1]")
	WebElement exchangeCodeTextField;
	@AndroidFindBy(name = "绑定")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[4]")
	WebElement bindCodeButton;
	@AndroidFindBy(name = "关闭")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[7]")
	WebElement closeButton;
	
	public BindInvestmentVolumePage(AppiumDriver<?> driver) {
		super(driver);
	}
	
	public void inputExchangeCodeAndBindCode(String code) {
		exchangeCodeTextField.clear();
		exchangeCodeTextField.sendKeys("123456");
		bindCodeButton.click();
		closeButton.click();
	}
}
