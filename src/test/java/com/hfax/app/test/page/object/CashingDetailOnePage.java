package com.hfax.app.test.page.object;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

import com.hfax.app.test.BaseTest;
public class CashingDetailOnePage extends BasePage {
	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[4]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[4]")
	WebElement balanceLabel;
	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[5]/android.widget.EditText[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[1]")
	WebElement amountText;
	@AndroidFindBy(name = "马上提现")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[2]")
	WebElement cashingBtn;
	@AndroidFindBy(name = "关闭")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[10]")
	WebElement closePoint;
	public CashingDetailOnePage(AppiumDriver<?> driver){
		super(driver);
	}
	public void goToCashingTest(String amount) throws Exception{
		String money = BaseTest.isAndroid() ? balanceLabel.getAttribute("name") : balanceLabel.getText();
		if (money.substring(10).equals("0")){
			amountText.clear();
			amountText.sendKeys(amount);
			cashingBtn.click();
			closePoint.click();
			
		}else{
			amountText.clear();
			amountText.sendKeys(amount);
			cashingBtn.click();
		}
				
	}
	
	public String haveMoney(){
		return (BaseTest.isAndroid() ? balanceLabel.getAttribute("name") : balanceLabel.getText()).substring(10);
	}
}
