package com.hfax.app.test.page.object;

import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class UserInvestmentVolumePage extends BasePage {

	@AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[1]/android.view.View[2]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[1]")
	WebElement investmentVolumeDetailLabel;
	
	public UserInvestmentVolumePage(AppiumDriver<?> driver) {
		super(driver);
	}
	
	public WebElement getInvestmentVolumeDetailLabel() {
		return investmentVolumeDetailLabel;
	}
}
