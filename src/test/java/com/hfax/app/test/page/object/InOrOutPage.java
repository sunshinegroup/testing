package com.hfax.app.test.page.object;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.Point;
public class InOrOutPage extends BasePage{
	@AndroidFindBy(name = "转入")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[1]")
	private WebElement inBtn;
	@AndroidFindBy(name = "转出")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[2]")
	private WebElement outBtn;
	@AndroidFindBy(xpath = "//android.view.View[1]/android.view.View[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[1]")
	private WebElement backBtn;
	public InOrOutPage(AppiumDriver<?> driver){
		super(driver);
	}
	public void gotoOutDetail(){
		outBtn.click();
	}
	public void gotoInDetail(){
		inBtn.click();
	}
	public void gotoBack(){
		Point location = backBtn.getLocation();
		driver.tap(1, location.x, location.y, 100);
		driver.performTouchAction(new TouchAction(driver).tap(location.getX(), location.getY()));
	}
}
