package com.hfax.app.test.page.object;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

import java.util.List;

import org.apache.xpath.operations.Bool;
import org.openqa.selenium.WebElement;

import com.hfax.app.test.BaseTest;
public class UpdatePinCodePage extends BasePage  {
	@AndroidFindBy(xpath = "//android.view.View[4]/android.widget.EditText[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[1]")
	WebElement realNameText;
	@AndroidFindBy(xpath = "//android.view.View[4]/android.widget.EditText[2]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[2]")
	WebElement idCardText;
	@AndroidFindBy(xpath = "//android.view.View[4]/android.widget.EditText[3]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[3]")
	WebElement bankCodeText;
	@AndroidFindBy(name = "获取验证码")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[6]")
	WebElement getMessageCodeBtn;
	@AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[1]/android.view.View[4]/android.view.View[1]/android.view.View[1]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[3]/UIAStaticText[1]")
	WebElement getMessageCodeBtn2;//未绑卡时的获取验证码
	@AndroidFindBy(name = "关闭")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[4]")
	WebElement closePoint;//未绑卡时点击获取验证码提示框

	@AndroidFindBy(xpath = "//android.view.View[4]/android.widget.EditText[4]")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[4]")
	WebElement messageCodeText;
	@AndroidFindBy(name = "下一步")
	@iOSFindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[2]/UIAStaticText[1]")
	WebElement nextBtn;
	@AndroidFindBy(className = "android.widget.EditText")
	//@iOSFindBy(xpath = "//")
	List<WebElement> editTexts; //获取页面输入框的个数来判断是绑卡还是没有绑卡
	
	public UpdatePinCodePage(AppiumDriver<?> driver){
		super(driver);
	}
	
	public boolean editTextSize(){
		if(BaseTest.isAndroid()){
			return editTexts.size() <2 ? true : false;
		}else{
			return getMessageCodeBtn2 != null ? true : false;
		}
	}
	
	public void goToUpdatePassWordTest(String realName,String idCard,String bankCode,String code ){
		
		if(editTextSize()){
			getMessageCodeBtn2.click();
			closePoint.click();
			realNameText.clear();
			realNameText.sendKeys(code);
			nextBtn.click();	
		}
		else{
			realNameText.clear();
			realNameText.sendKeys(realName);
			idCardText.clear();
			idCardText.sendKeys(idCard);
			bankCodeText.clear();
			bankCodeText.sendKeys(bankCode);
			getMessageCodeBtn.click();
			closePoint.click();
			messageCodeText.sendKeys(code);
			nextBtn.click();
			closePoint.click();
		}
		
	}
}
