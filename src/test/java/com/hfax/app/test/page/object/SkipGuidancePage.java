package com.hfax.app.test.page.object;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;

public class SkipGuidancePage extends BasePage {

	public SkipGuidancePage(AppiumDriver<?> driver) {
		super(driver);
	}

	public void skipGuidancePage() {
		WebElement slider = driver.findElementByClassName("android.widget.LinearLayout");
		Point sliderLocation = getCenter(slider);
		int startX = sliderLocation.getX() * 2 - 50;
		int startY = sliderLocation.getY();
		int endX = sliderLocation.getX() / 10;
		int endY = sliderLocation.getY();
		driver.swipe(startX, startY, endX, endY, 500);
		driver.swipe(startX, startY, endX, endY, 500);
		driver.swipe(startX, startY, endX, endY, 500);
		slider.click();
	}

	public Point getCenter(WebElement element) {
		Point upperLeft = element.getLocation();
		Dimension dimensions = element.getSize();
		return new Point(upperLeft.getX() + dimensions.getWidth() / 2, upperLeft.getY() + dimensions.getHeight() / 2);
	}
}