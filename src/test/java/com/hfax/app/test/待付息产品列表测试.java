package com.hfax.app.test;

import java.util.concurrent.TimeUnit;

import com.hfax.app.test.object.UserManager;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.PurchasedProductsPage;
import com.hfax.app.test.page.object.StartPage;
import static junit.framework.TestCase.assertTrue;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class 待付息产品列表测试 extends BaseTest {

	private StartPage mStartPage;
	private LoginPage mLoginPage;
	private HomePage mHomePage;
	private PurchasedProductsPage mPurchasedProductsPage;
	
	public void setUp() throws Exception {
		super.setUp();
		
		mStartPage = new StartPage(driver);
	    mLoginPage = new LoginPage(driver);
	    mHomePage = new HomePage(driver);
	    mPurchasedProductsPage = new PurchasedProductsPage(driver);
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
	}
	
	@Test
	public void testPayInterestProducts() throws Exception {
		super.skipGuidancePage();
		
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mStartPage);
        mStartPage.goToLoginView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mLoginPage);
        mLoginPage.login(UserManager.INSTANCE.userWithCertification);
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mHomePage);
        mHomePage.goToExpectedEarningDetailView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mPurchasedProductsPage);
        WebElement payInterestProductsButton = mPurchasedProductsPage.getPayInterestProductsButton();
		assertTrue(payInterestProductsButton.getText().equals("待付息产品") || payInterestProductsButton.getAttribute("name").equals("待付息产品"));
        WebElement finishedProductsButton = mPurchasedProductsPage.getFinishedProductsButton();
		assertTrue(finishedProductsButton.getText().equals("已完成产品") || finishedProductsButton.getAttribute("name").equals("已完成产品"));        
	}

}
