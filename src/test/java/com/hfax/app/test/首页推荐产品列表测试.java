package com.hfax.app.test;

import com.hfax.app.test.object.UserManager;
import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.StartPage;
import com.hfax.app.test.page.object.WealthManagementPage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertTrue;

public class 首页推荐产品列表测试 extends BaseTest {
	
	private StartPage mStartPage;
	private LoginPage mLoginPage;
	private HomePage mHomePage;
	private WealthManagementPage mWealthManagementPage;
    private boolean noRecord;
    
    public void setUp() throws Exception {
    	super.setUp();
    	
    	mStartPage = new StartPage(driver);
        mLoginPage = new LoginPage(driver);
        mHomePage = new HomePage(driver);
        mWealthManagementPage = new WealthManagementPage(driver);
    }
    
    public void tearDown() throws Exception {
    	super.tearDown();
    }
    
    @Test
    public void testRecommendProducts()  throws Exception {
    	super.skipGuidancePage();
    	
    	PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mStartPage);
        mStartPage.goToLoginView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(60, TimeUnit.SECONDS)), mLoginPage);
        mLoginPage.login(UserManager.INSTANCE.userWithCertification);
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mHomePage);
        List<WebElement> noRecordLabel = mHomePage.getNoRecordLabel();
        if (noRecordLabel.size() > 0) {
            if (BaseTest.isIOS()) {
                noRecord = noRecordLabel.get(0).getText().equals("暂无记录");
            } else {
                noRecord = noRecordLabel.get(0).getAttribute("name").equals("暂无记录");
            }
        }
        if (!noRecord) {
            mHomePage.goToMoreProductsView();
            PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mWealthManagementPage);
            List<WebElement> productsList = mWealthManagementPage.getProductsList();
            WebElement lastLink = productsList.get(0);
            assertTrue(lastLink.getText().equals("抢购") || lastLink.getAttribute("name").equals("抢购"));
        }
    }

}
