package com.hfax.app.test;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import com.hfax.app.test.page.object.StartPage;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

/**
 * @author WangSummer
 *
 */
public class 权限测试 extends BaseTest {
	private StartPage mStartPage;

	public void setUp() throws Exception {
		super.setUp();
		mStartPage = new StartPage(driver);
	}

	@Test
	public void allow() {
		super.skipGuidancePage();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)),
				mStartPage);
		mStartPage.goToLoginView();
	}

}
