package com.hfax.app.test;

import java.util.concurrent.TimeUnit;

import com.hfax.app.test.object.UserManager;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.MessagePage;
import com.hfax.app.test.page.object.StartPage;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class 消息列表 extends BaseTest {

	private MessagePage mMessagePage;
	private StartPage mStartPage;
	private LoginPage mLoginPage;

	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		mMessagePage = new MessagePage(driver);
		mStartPage = new StartPage(driver);
		mLoginPage = new LoginPage(driver);
	}

	@Test
	public void messageTest() throws Exception {
		super.skipGuidancePage();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)),
				mStartPage);
		mStartPage.goToLoginView();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)),
				mLoginPage);
		mLoginPage.login(UserManager.INSTANCE.userWithCertification);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)),
				mMessagePage);
		mMessagePage.message();
	}
}
