package com.hfax.app.test;

import com.hfax.app.test.object.UserManager;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.MenuPage;
import com.hfax.app.test.page.object.StartPage;
import com.hfax.app.test.page.object.FeedBackPage;

public class 意见反馈测试 extends BaseTest {
	
	private StartPage mStartPage;
	private LoginPage mLoginPage;
	private HomePage mHomePage;
	private MenuPage mMenuPage;
	private FeedBackPage mFeedBackPage;
    
	public void setUp() throws Exception {
		super.setUp();
		
		mStartPage = new StartPage(driver);
	    mLoginPage = new LoginPage(driver);
	    mHomePage = new HomePage(driver);
	    mMenuPage = new MenuPage(driver);
	    mFeedBackPage = new FeedBackPage(driver);
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
	}
	
	@Test
	public void testFeedBack() throws Exception {
		super.skipGuidancePage();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mStartPage);
        mStartPage.goToLoginView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mLoginPage);
        mLoginPage.login(UserManager.INSTANCE.userWithCertification);
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mHomePage);
        mHomePage.goToMenuView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mMenuPage);
        mMenuPage.goToFeedBackView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mFeedBackPage);
        mFeedBackPage.inputSuggestionAndSubmit("testing ");
	}

}
