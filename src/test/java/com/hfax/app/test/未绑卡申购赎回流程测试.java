package com.hfax.app.test;

/**
 * Created by zhangchi on 16/3/25.
 */
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.hfax.app.test.object.UserManager;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.hfax.app.test.page.object.AppliedForDetailOnePage;
import com.hfax.app.test.page.object.AppliedForDetailTwoPage;
import com.hfax.app.test.page.object.AppliedForProfileDetailPage;
import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.PasswordPage;
import com.hfax.app.test.page.object.RechargePage;
import com.hfax.app.test.page.object.StartPage;
import com.hfax.app.test.page.object.TiedCardPage;


import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

import static junit.framework.TestCase.assertTrue;

public class 未绑卡申购赎回流程测试 extends BaseTest{
    private StartPage mStartPage;
    private LoginPage mLoginPage;
    private HomePage mHomePage;
    private PasswordPage mPasswordPage;

    private AppliedForDetailOnePage mAppliedForDetailOnePage;
    private AppliedForDetailTwoPage mAppliedForDetailTwoPage;
    private AppliedForProfileDetailPage mAppliedForProfileDetailPage;
    private RechargePage mRechargePage;
    private TiedCardPage mTiedCardPage;

    private float inputAmount;
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        mStartPage = new StartPage(driver);
        mLoginPage = new LoginPage(driver);
        mHomePage = new HomePage(driver);
        mPasswordPage = new PasswordPage(driver);
        mAppliedForDetailOnePage = new AppliedForDetailOnePage(driver);
        mAppliedForProfileDetailPage = new AppliedForProfileDetailPage(driver);
        mAppliedForDetailTwoPage = new AppliedForDetailTwoPage(driver);
        mRechargePage = new RechargePage(driver);
        mTiedCardPage = new TiedCardPage(driver);

    }

    @Test
    public void testAppliedForTest() throws Exception {
        super.skipGuidancePage();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)),
                mStartPage);
        mStartPage.goToLoginView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)),
                mLoginPage);
        mLoginPage.login(UserManager.INSTANCE.userWithCertification);
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mHomePage);
        mHomePage.goToAppliedForTest();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForDetailOnePage);
        mAppliedForDetailOnePage.goToProfileTest();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForProfileDetailPage);
        mAppliedForProfileDetailPage.goToBack();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForDetailOnePage);
        mAppliedForDetailOnePage.goToBrochureTest();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForProfileDetailPage);
        mAppliedForProfileDetailPage.goToBack();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForDetailOnePage);
        mAppliedForDetailOnePage.goToTipsTest();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForProfileDetailPage);
        mAppliedForProfileDetailPage.goToBack();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForDetailOnePage);
        mAppliedForDetailOnePage.goToContractTest();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForProfileDetailPage);
        mAppliedForProfileDetailPage.goToBack();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForDetailOnePage);
        mAppliedForDetailOnePage.goToAppliedForDetailTwoTest();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForDetailTwoPage);
        mAppliedForDetailTwoPage.FillAmountTest(900);
        inputAmount = 900;
        mAppliedForDetailTwoPage.AddVoucherTest();
        mAppliedForDetailTwoPage.AddRaiseTest();
        mAppliedForDetailTwoPage.ConfirmBtnTest();
        //关闭实名认证提示
        mAppliedForDetailTwoPage.closePoint();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mTiedCardPage);
        //实名认证
        mTiedCardPage.tiedCard(UserManager.INSTANCE.userWithoutCertification);
        mPasswordPage.inputPassword();
        mPasswordPage.inputPassword();
        //绑卡页面
        mTiedCardPage.bindingBank(UserManager.INSTANCE.userForRegister.getCardNO());
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mHomePage);
        //绑卡成功之后跳回首页重新申购
        mHomePage.goToAppliedForTest();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForDetailOnePage);
        mAppliedForDetailOnePage.goToProfileTest();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForProfileDetailPage);
        mAppliedForProfileDetailPage.goToBack();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForDetailOnePage);
        mAppliedForDetailOnePage.goToBrochureTest();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForProfileDetailPage);
        mAppliedForProfileDetailPage.goToBack();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForDetailOnePage);
        mAppliedForDetailOnePage.goToTipsTest();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForProfileDetailPage);
        mAppliedForProfileDetailPage.goToBack();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForDetailOnePage);
        mAppliedForDetailOnePage.goToContractTest();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForProfileDetailPage);
        mAppliedForProfileDetailPage.goToBack();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForDetailOnePage);
        mAppliedForDetailOnePage.goToAppliedForDetailTwoTest();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
                mAppliedForDetailTwoPage);
        mAppliedForDetailTwoPage.FillAmountTest(900);
        inputAmount = 900;
        mAppliedForDetailTwoPage.AddVoucherTest();
        mAppliedForDetailTwoPage.AddRaiseTest();
        if(mAppliedForDetailTwoPage.compare()) {
            mAppliedForDetailTwoPage.ConfirmBtnTest();
        }
        mRechargePage.rechargeButton();
        mPasswordPage.inputPassword();
        mAppliedForDetailTwoPage.finish();
        //调用查看已购买列表
        String investTotalAmount =  mHomePage.getInvestTotalAmount();
        assertTrue(Integer.valueOf(investTotalAmount) == inputAmount);
        mHomePage.goToPurchasedProductsView();
        List<WebElement> purchasedProudctsList = mHomePage.getPurchasedProudctsList();
        WebElement firstLink = purchasedProudctsList.get(0);
        assertTrue( firstLink.getText().equals("募资中") || firstLink.getAttribute("name").equals("募资中"));
    }
}
