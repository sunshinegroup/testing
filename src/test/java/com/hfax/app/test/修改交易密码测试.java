package com.hfax.app.test;

import com.hfax.app.test.object.UserManager;
import com.hfax.app.test.page.object.LoginPage;

import com.hfax.app.test.page.object.StartPage;
import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.MenuPage;
import com.hfax.app.test.page.object.PassWordManagerPage;
import com.hfax.app.test.page.object.PasswordPage;

import com.hfax.app.test.page.object.UpdatePinCodePage;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import java.util.concurrent.TimeUnit;

public class 修改交易密码测试 extends BaseTest{
	private StartPage mStartPage;
	private LoginPage mLoginPage;
	private HomePage mHomePage;
	private MenuPage mMenuPage;
	private PassWordManagerPage mPassWordManagerPage;
	private PasswordPage mPasswordPage;

	private UpdatePinCodePage mUpdatePinCodePage;
	
	@Override
    @Before
	public void setUp() throws Exception {
        super.setUp();
        mStartPage = new StartPage(driver);
        mLoginPage = new LoginPage(driver);
        mHomePage = new HomePage(driver);
        mMenuPage = new MenuPage(driver);
        mPassWordManagerPage = new PassWordManagerPage(driver);
        mUpdatePinCodePage = new UpdatePinCodePage(driver); 
        mPasswordPage = new PasswordPage(driver);
    }
	
	@Test
	 public void testUpdatePinCodeTest() throws Exception {
		 super.skipGuidancePage();
	     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mStartPage);
	     mStartPage.goToLoginView();
	     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mLoginPage);
	     mLoginPage.login(UserManager.INSTANCE.userWithCertification);
	     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mHomePage);
	     mHomePage.goToMenuView();
	     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mMenuPage);
	     mMenuPage.gotoPasswordManager();
	     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mPassWordManagerPage);
	     mPassWordManagerPage.goToUpdatePinCodeTest();
	     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mUpdatePinCodePage);
		 mUpdatePinCodePage.goToUpdatePassWordTest("123456", "1231", "12321","1234");
//	     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mPasswordPage);
//	     mPasswordPage.inputPassword();

		 
	}
}
