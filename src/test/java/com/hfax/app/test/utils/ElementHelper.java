package com.hfax.app.test.utils;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;

/**
 * Created by trila on 16/3/10.
 */
public class ElementHelper {
    static public Point getCenter(WebElement element) {
        Point upperLeft = element.getLocation();
        Dimension dimensions = element.getSize();
        return new Point(upperLeft.getX() + dimensions.getWidth() / 2, upperLeft.getY() + dimensions.getHeight() / 2);
    }
    
//    static public WebElement findBackButton(AppiumDriver<?> driver) {
//		return findBackButton(driver, "//android.view.View[1]/android.view.View[1]");
//    }
//    
//    static public WebElement findBackButton(AppiumDriver<?> driver, String path) {
//    	return driver.findElementByXPath(path);
//    }
    
    
}
