package com.hfax.app.test;

import java.util.concurrent.TimeUnit;

import com.hfax.app.test.object.UserManager;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.MenuPage;
import com.hfax.app.test.page.object.PasswordPage;
import com.hfax.app.test.page.object.RechargePage;
import com.hfax.app.test.page.object.StartPage;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class 充值测试 extends BaseTest {
	private StartPage mStartPage;
	private LoginPage mLoginPage;
	private HomePage mHomePage;
	private MenuPage mMenuPage;
	private RechargePage mRechargePage;
	private PasswordPage mPasswordPage;

	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		mStartPage = new StartPage(driver);
		mLoginPage = new LoginPage(driver);
		mRechargePage = new RechargePage(driver);
		mHomePage = new HomePage(driver);
		mMenuPage = new MenuPage(driver);
		mPasswordPage = new PasswordPage(driver);
	}

	@Test
	public void testRechargeTest() throws Exception {
		super.skipGuidancePage();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
				mStartPage);
		mStartPage.goToLoginView();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
				mLoginPage);
		mLoginPage.login(UserManager.INSTANCE.userWithCertification);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
				mHomePage);
		mHomePage.goToMenuView();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
				mMenuPage);
		mMenuPage.gotoRecharge();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
				mRechargePage);
		mRechargePage.recharge(100);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
				mPasswordPage);

		mPasswordPage.inputPassword();

		mRechargePage.successButton();
	}
}
