package com.hfax.app.test;

import com.hfax.app.test.object.UserManager;
import com.hfax.app.test.page.object.HomePage;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import com.hfax.app.test.page.object.ForgetPasswordPage;
import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.StartPage;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

import java.util.concurrent.TimeUnit;
public class 忘记密码测试 extends BaseTest {
	
	private StartPage mStartPage;
	private LoginPage mLoginPage;
	private ForgetPasswordPage mForgetPasswordPage;
	private HomePage mHomePage;
	
	public void setUp() throws Exception {  
		super.setUp();
		
		mStartPage = new StartPage(driver);
        mLoginPage = new LoginPage(driver);
        mForgetPasswordPage = new ForgetPasswordPage(driver);
		mHomePage = new HomePage(driver);
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
	}
	
	@Test 
	public void testForgetPassword() throws Exception {
		super.skipGuidancePage();		
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mStartPage);
        mStartPage.goToLoginView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mLoginPage);
        mLoginPage.goToForgetPasswordView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mForgetPasswordPage);
        mForgetPasswordPage.resetCode(UserManager.INSTANCE.userForResetPassword.getPhone(), UserManager.INSTANCE.userForResetPassword.getPassword());
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mHomePage);
		mHomePage.goToHomePageView();
	}
}
