package com.hfax.app.test;

import java.util.concurrent.TimeUnit;

import com.hfax.app.test.object.UserManager;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.MenuPage;
import com.hfax.app.test.page.object.SharePage;
import com.hfax.app.test.page.object.StartPage;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class 分享测试 extends BaseTest {
	private HomePage mHomePage;
	private MenuPage mMenuPage;
	private SharePage mSharePage;
	private StartPage mStartPage;
	private LoginPage mLoginPage;
	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		mSharePage = new SharePage(driver);
		mStartPage = new StartPage(driver);
		mLoginPage = new LoginPage(driver);
		mHomePage = new HomePage(driver);
		mMenuPage = new MenuPage(driver);
	}

	@Test
	public void messageTest() throws Exception{
		super.skipGuidancePage();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)),
				mStartPage);
		mStartPage.goToLoginView();

		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)),
				mLoginPage);
		mLoginPage.login(UserManager.INSTANCE.userWithCertification);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)),
				mHomePage);
		mHomePage.goToMenuView();

		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mMenuPage);
		mMenuPage.share();

		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)),
				mSharePage);
		mSharePage.shareTest();
	}
}