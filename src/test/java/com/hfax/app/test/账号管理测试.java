package com.hfax.app.test;

import com.hfax.app.test.object.User;
import com.hfax.app.test.object.UserManager;
import com.hfax.app.test.page.object.*;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.theories.suppliers.TestedOn;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by trila on 16/3/24.
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class 账号管理测试 extends BaseTest {
    private StartPage mStartPage;
    private LoginPage mLoginPage;
    private HomePage mHomePage;
    private RegisterPage mRegisterPage;
    private MenuPage mMenuPage;
    private PassWordManagerPage mPassWordManagerPage;
    private updatePassWordPage mUpdatePassWordPage;
    private ForgetPasswordPage mForgetPasswordPage;

    private User mUser;

    public void setUp() throws Exception {
        super.setUp();
        mStartPage = new StartPage(driver);
        mLoginPage = new LoginPage(driver);
        mHomePage = new HomePage(driver);
        mRegisterPage = new RegisterPage(driver);
        mMenuPage = new MenuPage(driver);
        mPassWordManagerPage = new PassWordManagerPage(driver);
        mUpdatePassWordPage = new updatePassWordPage(driver);
        mForgetPasswordPage = new ForgetPasswordPage(driver);
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }


    @Test
    public void test01Register() throws Exception {
        super.skipGuidancePage();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mStartPage);
        mStartPage.goToRegisterView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(60, TimeUnit.SECONDS)), mRegisterPage);
        mRegisterPage.register(UserManager.INSTANCE.userForRegister);
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mHomePage);
        mHomePage.goToMenuView();

        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mMenuPage);
        mMenuPage.logout();
    }

    @Test
    public void test02LoginAndModifyPassword() throws Exception {
        //登录验证注册成功
        super.skipGuidancePage();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mStartPage);
        mStartPage.goToLoginView();

        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mLoginPage);
        mLoginPage.login(UserManager.INSTANCE.userForRegister);

        //修改交易密码
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mHomePage);
        mHomePage.goToMenuView();

        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mMenuPage);
        mMenuPage.gotoPasswordManager();

        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mPassWordManagerPage);
        mPassWordManagerPage.goToUpdatePassWordTest();

        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mUpdatePassWordPage);
        mUpdatePassWordPage.goToUpdatePassWordTest("a111111111", "a111111112");
        mUpdatePassWordPage.closePopViewTest();

        //登出然后验证修改密码成功
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mMenuPage);
        mMenuPage.logout();

        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mLoginPage);
        mLoginPage.login(UserManager.INSTANCE.userForRegister);

        //登出然后开始忘记密码测试
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mHomePage);
        mHomePage.goToMenuView();

        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mMenuPage);
        mMenuPage.logout();

        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mStartPage);
        mStartPage.goToLoginView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mLoginPage);
        mLoginPage.goToForgetPasswordView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mForgetPasswordPage);
        mForgetPasswordPage.resetCode(UserManager.INSTANCE.userForResetPassword.getPhone(), UserManager.INSTANCE.userForResetPassword.getPassword());
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mHomePage);

        //验证忘记密码成功
        mHomePage.goToHomePageView();
    }
}
