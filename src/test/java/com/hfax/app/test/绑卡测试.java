package com.hfax.app.test;

import java.util.concurrent.TimeUnit;

import com.hfax.app.test.object.UserManager;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.MenuPage;
import com.hfax.app.test.page.object.PasswordPage;
import com.hfax.app.test.page.object.StartPage;
import com.hfax.app.test.page.object.TiedCardPage;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class 绑卡测试 extends BaseTest {
	private HomePage mHomePage;
	private MenuPage mMenuPage;
	private LoginPage mLoginPage;
	private StartPage mStartPage;
	private TiedCardPage mTiedCardPage;
	private PasswordPage mPasswordPage;
	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		mLoginPage = new LoginPage(driver);
		mHomePage = new HomePage(driver);
		mMenuPage = new MenuPage(driver);
		mStartPage = new StartPage(driver);
		mTiedCardPage = new TiedCardPage(driver);
		mPasswordPage = new PasswordPage(driver);

	}

	@Test
	public void testTiedCard() throws Exception{
		super.skipGuidancePage();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)),
				mStartPage);
		mStartPage.goToLoginView();

		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)),
				mLoginPage);
		mLoginPage.login(UserManager.INSTANCE.userWithoutCertification);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)),
				mHomePage);
		mHomePage.goToMenuView();

		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mMenuPage);
		mMenuPage.gotoBank();
		
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mTiedCardPage);
		mTiedCardPage.tiedCard(UserManager.INSTANCE.userWithoutCertification);
		
		mPasswordPage.inputPassword();
		mPasswordPage.inputPassword();

        mTiedCardPage.bindingBank(UserManager.INSTANCE.userForRegister.getCardNO());
	}
}
