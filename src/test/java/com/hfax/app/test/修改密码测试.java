package com.hfax.app.test;

import com.hfax.app.test.object.UserManager;
import com.hfax.app.test.page.object.LoginPage;

import com.hfax.app.test.page.object.StartPage;
import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.MenuPage;
import com.hfax.app.test.page.object.PassWordManagerPage;
import com.hfax.app.test.page.object.updatePassWordPage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import java.util.concurrent.TimeUnit;

public class 修改密码测试 extends BaseTest{
	private StartPage mStartPage;
	private LoginPage mLoginPage;
	private HomePage mHomePage;
	private MenuPage mMenuPage;
	private PassWordManagerPage mPassWordManagerPage;
	private updatePassWordPage mUpdatePassWordPage;
	@Override
    @Before
	public void setUp() throws Exception {
        super.setUp();
        mStartPage = new StartPage(driver);
        mLoginPage = new LoginPage(driver);
        mHomePage = new HomePage(driver);
        mMenuPage = new MenuPage(driver);
        mPassWordManagerPage = new PassWordManagerPage(driver);
        mUpdatePassWordPage = new updatePassWordPage(driver);
    }
	
 @Test
 public void testUpdatePasswordTest() throws Exception {
	 super.skipGuidancePage();
     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mStartPage);
     mStartPage.goToLoginView();
     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mLoginPage);
     mLoginPage.login(UserManager.INSTANCE.userWithCertification);
     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mHomePage);
     mHomePage.goToMenuView();
     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mMenuPage);
     mMenuPage.gotoPasswordManager();
     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mPassWordManagerPage);
     mPassWordManagerPage.goToUpdatePassWordTest();
     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mUpdatePassWordPage);
     mUpdatePassWordPage.goToUpdatePassWordTest("a111111111", "a111111112");
     mUpdatePassWordPage.closePopViewTest();
     mMenuPage.gotuHomePage();     //返回到首页面

 }
}
