package com.hfax.app.test;

import com.hfax.app.test.object.UserManager;
import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.StartPage;
import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.InOrOutPage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import java.util.concurrent.TimeUnit;

public class 转入转出页面测试 extends BaseTest{
	private StartPage mStartPage;
	private LoginPage mLoginPage;
	private HomePage mHomePage;
	private InOrOutPage mInOrOutPage;
	
	@Override
    @Before
	public void setUp() throws Exception {
        super.setUp();
        mStartPage = new StartPage(driver);
        mLoginPage = new LoginPage(driver);
        mHomePage = new HomePage(driver);
        mInOrOutPage = new InOrOutPage(driver);
    }
	
 @Test
 public void testInOrOutTest() throws Exception {
	 super.skipGuidancePage();
     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mStartPage);
     mStartPage.goToLoginView();
     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mLoginPage);
     mLoginPage.login(UserManager.INSTANCE.userWithCertification);
     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mHomePage);
     mHomePage.goToBalanceTest();
     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),mInOrOutPage);
     mInOrOutPage.gotoOutDetail();
     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),mInOrOutPage);
     mInOrOutPage.gotoInDetail();
     PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),mInOrOutPage);
	 mInOrOutPage.gotoBack();
	 PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)),
				mHomePage);
	}
}
