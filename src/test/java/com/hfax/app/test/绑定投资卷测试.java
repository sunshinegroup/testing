package com.hfax.app.test;

import com.hfax.app.test.object.UserManager;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import com.hfax.app.test.page.object.BindInvestmentVolumePage;
import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.InvestmentVolumePage;
import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.MenuPage;
import com.hfax.app.test.page.object.StartPage;
import java.util.concurrent.TimeUnit;

public class 绑定投资卷测试 extends BaseTest {
	

	private StartPage mStartPage;
	private LoginPage mLoginPage;
	private HomePage mHomePage;
	private MenuPage mMenuPage;
	private InvestmentVolumePage mInvestmentVolumePage;
	private BindInvestmentVolumePage mBindInvestmentVolumePage;
	
	public void setUp() throws Exception {
		super.setUp();
		mStartPage = new StartPage(driver);
	    mLoginPage = new LoginPage(driver);
	    mHomePage = new HomePage(driver);
	    mMenuPage = new MenuPage(driver);
	    mInvestmentVolumePage = new InvestmentVolumePage(driver);
	    mBindInvestmentVolumePage = new BindInvestmentVolumePage(driver);
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testBindInvestment() throws Exception {
		super.skipGuidancePage();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mStartPage);
        mStartPage.goToLoginView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mLoginPage);
        mLoginPage.login(UserManager.INSTANCE.userWithCertification);
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(60, TimeUnit.SECONDS)), mHomePage);
        mHomePage.goToMenuView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mMenuPage);
        mMenuPage.goToInvestmentVolumeView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mInvestmentVolumePage);
        mInvestmentVolumePage.goToBindInvestmentVolumeView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mBindInvestmentVolumePage);
        mBindInvestmentVolumePage.inputExchangeCodeAndBindCode("123456");
		
	}

}
