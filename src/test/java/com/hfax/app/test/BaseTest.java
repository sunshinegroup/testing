package com.hfax.app.test;


import com.hfax.app.test.page.object.SplashPage;
import com.hfax.app.test.utils.Platform;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertFalse;
import static junit.framework.TestCase.assertEquals;

/**
 * Created by philip on 2/24/16.
 */
@SuppressWarnings("unused")
public class BaseTest {
    protected AppiumDriver<WebElement> driver;

    static private Platform mPlatform;
    protected FluentWait<WebDriver> wait;
    private SplashPage mSplashpage;
    
    static public Boolean isIOS() {
        return mPlatform.equals(Platform.IOS);
    }

    static public Boolean isAndroid() {
        return mPlatform.equals(Platform.ANDROID);
    }

    @Before
    public void setUp() throws Exception {
        Map<String, String> env = System.getenv();

        String appPath = env.get("APP_PATH");
        assertEquals("App path is empty", appPath != null, true);

        File checkApkPath = new File(appPath);
        assertEquals("App file does not exist", checkApkPath.exists(), true);

        String serverPath = env.get("SERVER_PATH");

        if (serverPath == null || serverPath.isEmpty()) {
            serverPath = "http://127.0.0.1:4723/wd/hub";
        }

        final String platform = env.get("PLATFORM");

        assertFalse("NO platform specified", platform != null && platform.isEmpty());

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName","iPhone 6");
        capabilities.setCapability("platformName", platform);
        capabilities.setCapability("noSign", true);
        capabilities.setCapability("app", appPath);

        if (platform.toLowerCase().contains("android")) {
            driver = new AndroidDriver<>(new URL(serverPath), capabilities);
            mPlatform = Platform.ANDROID;
        } else {
            driver = new IOSDriver<>(new URL(serverPath), capabilities);
            mPlatform = Platform.IOS;
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        mSplashpage = new SplashPage(driver);

        this.wait = new WebDriverWait(driver, 5000).withTimeout(5, TimeUnit.SECONDS).pollingEvery(100, TimeUnit.MILLISECONDS).ignoring(StaleElementReferenceException.class);

    }

    @After
    public void tearDown() throws Exception {
        if (driver != null) {
            driver.quit();
        }
    }

    public void skipGuidancePage() {
    	PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mSplashpage);
    	mSplashpage.skipGuidancePage();	
    }
}
