package com.hfax.app.test.object;

/**
 * Created by trila on 16/3/21.
 */
public enum UserManager {
    INSTANCE;

    public User userForRegister = new User("13599000007", "a111111111");
    public User userForResetPassword = new User("13599000007", "a111111111");
    public User userWithoutCertification = new User("13599000007", "a111111111", "王二", "6216732233334445002", "230903198403110210");
    public User userWithCertification = new User("13599000010", "a111111111", "王二", "6216732233334445002", "230903198403110210");
}


