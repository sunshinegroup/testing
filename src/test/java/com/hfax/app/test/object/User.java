package com.hfax.app.test.object;

/**
 * Created by trila on 16/3/4.
 */
public class User {
    private String mPhone = "";
    private String mPassword = "";
    private String mName = "";
    private String mCardNO = "";
    private String mID = "";

    public User() {
    }

    public User(String mPhone, String mPassword) {
        this.mPhone = mPhone;
        this.mPassword = mPassword;
    }

    public User(String mPhone, String mPassword, String mName, String mCardNO, String mID) {
        this.mPhone = mPhone;
        this.mPassword = mPassword;
        this.mName = mName;
        this.mCardNO = mCardNO;
        this.mID = mID;
    }

    public String getPhone() {
        return mPhone;
    }

    public String getPassword() {
        return mPassword;
    }

    public String getName() {
        return mName;
    }

    public String getCardNO() {
        return mCardNO;
    }

    public String getID() {
        return mID;
    }
}
