package com.hfax.app.test;
import com.hfax.app.test.object.UserManager;
import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.StartPage;
import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.MenuPage;
import com.hfax.app.test.page.object.PasswordPage;

import com.hfax.app.test.page.object.CashingDetailOnePage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

/**
 * 提现
 * @author apple
 *
 */
public class 提现测试 extends BaseTest {
	private StartPage mStartPage;
	private LoginPage mLoginPage;
	private HomePage mHomePage;
	private MenuPage mMenuPage;
	private PasswordPage mPasswordPage;
	private CashingDetailOnePage mCashingDetailOnePage;
	@Override
    @Before
	public void setUp() throws Exception {
        super.setUp();
        mStartPage = new StartPage(driver);
        mLoginPage = new LoginPage(driver);
        mHomePage = new HomePage(driver);
        mHomePage = new HomePage(driver);
        mMenuPage = new MenuPage(driver);
        mPasswordPage  = new PasswordPage(driver);
        mCashingDetailOnePage = new CashingDetailOnePage(driver);
    }
	 @Test
	    public void testCashingTest() throws Exception {
		 super.skipGuidancePage();
         PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mStartPage);
         mStartPage.goToLoginView();
         PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mLoginPage);
         mLoginPage.login(UserManager.INSTANCE.userWithCertification);
         PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mHomePage);
         mHomePage.goToMenuView();
         PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mMenuPage);
         mMenuPage.gotoCashing();
         PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mCashingDetailOnePage);
		mCashingDetailOnePage.goToCashingTest("1");
		if(!mCashingDetailOnePage.haveMoney().equals("0")){
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mPasswordPage);
        mPasswordPage.inputPassword();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mPasswordPage);
		}
	}

}
