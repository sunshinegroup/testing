package com.hfax.app.test;

/**
 * Created by liubiaocong on 16/3/21.
 */


import com.hfax.app.test.object.UserManager;import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import java.util.concurrent.TimeUnit;
import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.StartPage;

public class 登录测试 extends BaseTest {
    private StartPage mStartPage;
    private  LoginPage mLoginPage;
    private HomePage mHomePage;

    public void setUp() throws Exception {
        super.setUp();
        mStartPage = new StartPage(driver);
        mLoginPage = new LoginPage(driver);
        mHomePage = new HomePage(driver);
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void testLogin() throws Exception {
        super.skipGuidancePage();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mStartPage);
        mStartPage.goToLoginView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mLoginPage);
        mLoginPage.login(UserManager.INSTANCE.userWithCertification);
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mHomePage);
        mHomePage.goToHomePageView();
    }
}
