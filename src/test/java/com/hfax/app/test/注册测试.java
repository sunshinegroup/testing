package com.hfax.app.test;


import java.util.concurrent.TimeUnit;
import com.hfax.app.test.object.UserManager;
import com.hfax.app.test.page.object.HomePage;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import com.hfax.app.test.page.object.RegisterPage;
import com.hfax.app.test.page.object.StartPage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class 注册测试 extends BaseTest {
	
	private StartPage mStartPage;
	private RegisterPage mRegisterPage;
	private HomePage mHomePage;

	public void setUp() throws Exception {
		super.setUp();
		
		mStartPage = new StartPage(driver);
		mRegisterPage = new RegisterPage(driver);
		mHomePage = new HomePage(driver);
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
	}
	
	@Test 
	public void testRegister() throws Exception {
		super.skipGuidancePage();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mStartPage);
        mStartPage.goToRegisterView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(60, TimeUnit.SECONDS)), mRegisterPage);
        mRegisterPage.register(UserManager.INSTANCE.userForRegister);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mHomePage);
		mHomePage.goToHomePageView();
	}

}
