package com.hfax.app.test;

import com.hfax.app.test.object.UserManager;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import static junit.framework.TestCase.assertEquals;
import java.util.concurrent.TimeUnit;
import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.MenuPage;
import com.hfax.app.test.page.object.StartPage;


public class 登出测试 extends BaseTest {
	private StartPage mStartPage;
	private LoginPage mLoginPage;
	private HomePage mHomePage;
	private MenuPage mMenuPage;
	
	public void setUp() throws Exception {
		super.setUp();
		
		mStartPage = new StartPage(driver);
	    mLoginPage = new LoginPage(driver);
	    mHomePage = new HomePage(driver);
	    mMenuPage = new MenuPage(driver);
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testLogout() throws Exception {
		super.skipGuidancePage();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mStartPage);
        mStartPage.goToLoginView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mLoginPage);
        mLoginPage.login(UserManager.INSTANCE.userWithCertification);
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mHomePage);
        mHomePage.goToMenuView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mMenuPage);
        mMenuPage.logout();
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mStartPage);
		assertEquals("登录", mStartPage.getLoginButton().getText());
		assertEquals("注册", mStartPage.getRegisterButton().getText());
	}
}
