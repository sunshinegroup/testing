package com.hfax.app.test;

/**
 * Created by zhangchi on 16/3/28.
 */
import java.util.concurrent.TimeUnit;

import com.hfax.app.test.object.UserManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.MenuPage;
import com.hfax.app.test.page.object.PasswordPage;
import com.hfax.app.test.page.object.StartPage;
import com.hfax.app.test.page.object.RechargePage;
import com.hfax.app.test.page.object.CashingDetailOnePage;
import com.hfax.app.test.page.object.PassWordManagerPage;
import com.hfax.app.test.page.object.UpdatePinCodePage;
import com.hfax.app.test.page.object.BankCardListPage;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
public class 充值绑卡流程测试_已绑卡 extends BaseTest {
    private HomePage mHomePage;
    private MenuPage mMenuPage;
    private LoginPage mLoginPage;
    private StartPage mStartPage;
    private PasswordPage mPasswordPage;
    private RechargePage mRechargePage;
    private CashingDetailOnePage mCashingDetailOnePage;
    private PassWordManagerPage mPassWordManagerPage;
    private BankCardListPage mBankCardListPage;

    private UpdatePinCodePage mUpdatePinCodePage;

    private float inputAmount;
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        mLoginPage = new LoginPage(driver);
        mHomePage = new HomePage(driver);
        mMenuPage = new MenuPage(driver);
        mStartPage = new StartPage(driver);
        mPasswordPage = new PasswordPage(driver);
        mRechargePage = new RechargePage(driver);
        mCashingDetailOnePage = new CashingDetailOnePage(driver);
        mPassWordManagerPage = new PassWordManagerPage(driver);
        mUpdatePinCodePage = new UpdatePinCodePage(driver);
        mBankCardListPage = new BankCardListPage(driver);


    }
    @Test
    public void test() throws Exception {
        super.skipGuidancePage();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)),
                mStartPage);
        mStartPage.goToLoginView();

        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)),
                mLoginPage);
        mLoginPage.login(UserManager.INSTANCE.userWithoutCertification);
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)),
                mHomePage);
        mHomePage.goToMenuView();

        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mMenuPage);
        mMenuPage.gotoBank();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mBankCardListPage);
        //验证卡号
        mBankCardListPage.checkCardNum("1111****1111");
        //充值
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
                mHomePage);
        mHomePage.goToMenuView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
                mMenuPage);
        mMenuPage.gotoRecharge();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
                mRechargePage);
        mRechargePage.recharge(100);
        inputAmount = 100;
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
                mPasswordPage);

        mPasswordPage.inputPassword();

        mRechargePage.successButton();
        //验证余额
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
                mHomePage);
        Assert.assertTrue(mHomePage.compare(inputAmount));
        //提现
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mHomePage);
        mHomePage.goToMenuView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mMenuPage);
        mMenuPage.gotoCashing();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mCashingDetailOnePage);
        mCashingDetailOnePage.goToCashingTest("1");
        inputAmount = 1;
        if(!mCashingDetailOnePage.haveMoney().equals("0")) {
            PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mPasswordPage);
            mPasswordPage.inputPassword();
            PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mPasswordPage);
        }
        //验证余额
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
                mHomePage);
        Assert.assertTrue(mHomePage.compare(inputAmount));
        //修改交易密码
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mHomePage);
        mHomePage.goToMenuView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mMenuPage);
        mMenuPage.gotoPasswordManager();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mPassWordManagerPage);
        mPassWordManagerPage.goToUpdatePinCodeTest();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mUpdatePinCodePage);
        mUpdatePinCodePage.goToUpdatePassWordTest("123456", "1231", "12321","1234");
        //充值
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
                mHomePage);
        mHomePage.goToMenuView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
                mMenuPage);
        mMenuPage.gotoRecharge();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
                mRechargePage);
        mRechargePage.recharge(100);
        inputAmount = 100;
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
                mPasswordPage);

        mPasswordPage.inputPassword();

        mRechargePage.successButton();
        //验证余额
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
                mHomePage);
        Assert.assertTrue(mHomePage.compare(inputAmount));
        //提现
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mHomePage);
        mHomePage.goToMenuView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mMenuPage);
        mMenuPage.gotoCashing();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mCashingDetailOnePage);
        mCashingDetailOnePage.goToCashingTest("1");
        inputAmount = 1;
        if(!mCashingDetailOnePage.haveMoney().equals("0")) {
            PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mPasswordPage);
            mPasswordPage.inputPassword();
            PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mPasswordPage);
        }
        //验证余额
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)),
                mHomePage);
        Assert.assertTrue(mHomePage.compare(inputAmount));
        //验证消息列表
    }
}
