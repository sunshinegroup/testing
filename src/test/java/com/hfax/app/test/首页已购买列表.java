package com.hfax.app.test;

import com.hfax.app.test.object.UserManager;
import com.hfax.app.test.page.object.HomePage;
import com.hfax.app.test.page.object.LoginPage;
import com.hfax.app.test.page.object.StartPage;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import static junit.framework.TestCase.assertTrue;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class 首页已购买列表 extends BaseTest {
	
	private StartPage mStartPage;
	private LoginPage mLoginPage;
	private HomePage mHomePage;
	private boolean noRecord;
    private  boolean haveRecord;
    
    public void setUp() throws Exception {
    	super.setUp();
    	
    	mStartPage = new StartPage(driver);
        mLoginPage = new LoginPage(driver);
        mHomePage = new HomePage(driver);
    }
    
    public void tearDown() throws Exception {
    	super.tearDown();
    }
    
    @Test
    public void testPurchasedProductsTest() throws Exception {
    	super.skipGuidancePage();
    	
    	PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mStartPage);
        mStartPage.goToLoginView();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(10, TimeUnit.SECONDS)), mLoginPage);
        mLoginPage.login(UserManager.INSTANCE.userWithCertification);
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(30, TimeUnit.SECONDS)), mHomePage);
        mHomePage.goToPurchasedProductsView();
        List<WebElement> noRecordLabel = mHomePage.getNoRecordLabel();
        List<WebElement> purchasedProudctsList = mHomePage.getPurchasedProudctsList();

        if (noRecordLabel.size() > 0) {
        	WebElement firstLabel= noRecordLabel.get(0);
            if (BaseTest.isIOS()) {
                noRecord = firstLabel.getText().equals("暂无记录");
            } else {
                noRecord = firstLabel.getAttribute("name").equals("暂无记录");
            }
        } else {
        	WebElement firstLink = purchasedProudctsList.get(0);
            if (BaseTest.isIOS()) {
                haveRecord = firstLink.getText().equals("募资中");
            } else {
                haveRecord = firstLink.getAttribute("name").equals("募资中");
            }
        }
        assertTrue( noRecord || haveRecord);
    }

}
