package com.hfax.selenium.smoke;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by philip on 2/24/16.
 */
public class SmokeTest {
    private WebDriver driver = null;

    @Before
    public void setup() {
        driver = new FirefoxDriver();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testMainPage() throws Exception {
        driver.get("https://www.hfax.com");

        String titleString = driver.getTitle();
        assertEquals("Title not matching", titleString, "惠金所-阳光保险集团成员");
    }
}
